clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'MixingStudy3';
Rotor = 'Rotor1';




if strcmp(Rotor, 'Rotor1')
    RotorDiameter = 0.080;
    CellDiameter = 0.098;
    RotorLength = 0.138;
    CellVolume=0.3627951/997.561;
else
    RotorDiameter = 0.050;
    CellDiameter = 0.098;
    RotorLength = 0.138;
end

density = 997.561;
dynamicViscosity = 8.82E-07*density;
kineticViscosity = dynamicViscosity/density;



RPM500 = 500;
% FlowRatesString = {'1xFlow','2xFlow'};
FlowRates500 = [8.3, 16.6];
%FlowRates = [8.3];

ResidenceTime= CellVolume./(FlowRates500/(60*1e6));

RPMs = [250,1000];




model = 'LES2'; %LES / RST
ion = 'mNa_1+'; %mNa_1+ /mSO4_2-
% RPM = 1000;
% FlowRatesString = {'2xFlow'};
% FlowRates = [16.6];

for i = 1:length(FlowRates500)
    dataFilesSim{i} = sprintf('./Data/Mixing/%s/%iRPM/%1.1fml/MassFlowAveragedOutletConcentrationof%sMonitor.csv',model, RPM500, FlowRates500(i), ion);
    dataFilesExp{i} = sprintf('./Data/Mixing/EXP/%iRPM/%1.1fml/Mixing.csv',RPM500,FlowRates500(i));
end

for i = 1:length(RPMs)
    dataFilesSim{length(FlowRates500) + i} = sprintf('./Data/Mixing/%s/%iRPM/%1.1fml/MassFlowAveragedOutletConcentrationof%sMonitor.csv',model, RPMs(i), FlowRates500(2), ion);
    dataFilesExp{length(FlowRates500) + i} = sprintf('./Data/Mixing/EXP/%iRPM/%1.1fml/Mixing.csv',RPMs(i),FlowRates500(2));
end


%SimConcentration = 0.00113730770584103*2;
ExpConcentration = 0.00113730770584103; % 2


SimConcentration = [0.0119146015602731 0.0119146015602731];
%ExpConcentration = 0.0119146015602731; %1

calConcentration = 0.1;
data = {};

Omega = RPM500/60*2*pi;

%% Extract Data
for i = 1:length(FlowRates500)
    data(i).Simulations = importOutflowConcentration3(dataFilesSim{i});
    
    data(i).Experimental = importExperimentalConcentrations2(dataFilesExp{i});
    
    
    data(i).FlowRate = FlowRates500(i);
    data(i).RPM = RPM500;
    Omega = data(i).RPM/60*2*pi;
    data(i).ReynoldsNumber = Omega*0.5*RotorDiameter*(0.5*(CellDiameter-RotorDiameter))/kineticViscosity;
    
    
    data(i).Sim.tHat = data(i).Simulations.Time / 1 ;
    data(i).Sim.mHat = data(i).Simulations.Concentration / SimConcentration(i);
    
    data(i).Exp.tHat=data(i).Experimental.Time / 1;
    data(i).Exp.mHat=data(i).Experimental.Concentration / ExpConcentration;
    
end


for i = (1:length(RPMs)) + length(FlowRates500)
    %     data(i).Simulations = importOutflowConcentration3(dataFilesSim{1});
    
    data(i).Experimental = importExperimentalConcentrations2(dataFilesExp{i});
    
    
    data(i).FlowRate = FlowRates500(2);
    data(i).RPM = RPMs(i-length(FlowRates500));
    Omega = data(i).RPM/60*2*pi;
    data(i).ReynoldsNumber = Omega*0.5*RotorDiameter*(0.5*(CellDiameter-RotorDiameter))/kineticViscosity;
    
    data(i).Sim.tHat = [];
    data(i).Sim.mHat = [];
    
    data(i).Exp.tHat=data(i).Experimental.Time / 1;
    data(i).Exp.mHat=data(i).Experimental.Concentration / ExpConcentration;
    
end

%% Fixx

% l1 = 8000;% length(data(1).Sim.mHat);
% t1 = data(1).Sim.tHat(l1);
% [~, i_12]= min(abs(data(2).Sim.tHat-t1));
% l2 = length(data(2).Sim.mHat);
% diff = data(1).Sim.mHat(l1) - data(2).Sim.mHat(i_12);
% ddiffdt = diff/data(2).Sim.tHat(l1);
% 
% dt = data(2).Sim.tHat((i_12+1):l2);
% data(1).Sim.mHat = [data(1).Sim.mHat(1:l1); data(2).Sim.mHat((i_12+1):l2)*0.9 + diff * 1.0 + dt*ddiffdt*0.1 ];
% data(1).Sim.tHat = [data(1).Sim.tHat(1:l1); data(2).Sim.tHat((i_12+1):l2)];

%% Transform
for i = 1:length(data)
    
    if (~isempty(data(i).Sim.tHat))
        %t_cal_index = min(find(data(i).Exp.mHat>calConcentration));
        %t_cal = 0;% data(i).Exp.tHat(t_cal_index);
        %m_cal = 0; % data(i).Exp.mHat(t_cal_index);
        
        m_cal =data(i).Exp.mHat(1);
        
        t_cal_sim_index = min(find(data(i).Sim.mHat > m_cal));
        t_cal_sim = data(i).Sim.tHat(t_cal_sim_index);
        
        %     data(i).tHat_sim = data(i).Simulations.tHat(t_cal_sim_index:end) - t_cal_sim;
        %     data(i).mHat_sim = data(i).Simulations.mHat(t_cal_sim_index:end);
        %
        %     data(i).tHat_exp = data(i).Experimental.tHat(t_cal_index:end) - t_cal;
        %     data(i).mHat_exp = data(i).Experimental.mHat(t_cal_index:end);
        
        
        data(i).tHat_sim = data(i).Sim.tHat(1:end)  - t_cal_sim;
        data(i).mHat_sim = data(i).Sim.mHat(1:end);
        
    else
        data(i).tHat_sim = [];
        data(i).mHat_sim = [];
    end
    
    data(i).tHat_exp = data(i).Exp.tHat(1:end) ;
    data(i).mHat_exp = data(i).Exp.mHat(1:end) ;
    
end

%% perfectly stirred

perfectlyStirred(1).tHat = data(1).Sim.tHat;
perfectlyStirred(1).mHat = 1-exp(-data(1).Sim.tHat/ResidenceTime(1)*0.25);





%% Plot
legendPos = 'no';
figureName = 'b';

paperfactorX = 0.55*1.25;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',8,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbolsExp = {'xr', '+b', '*g' , 'om'};
symbolsSim = {'-r', '--b'};

hold on
% plot(data(1).tHat_sim, data(1).mHat_sim, symbolsSim{1},'linewidth',1.0, 'DisplayName', sprintf('RST %1.1f mL/min, Re = %.1fx$10^4$',data(1).FlowRate, data(1).ReynoldsNumber*1e-4))


%plot(perfectlyStirred(1).tHat, perfectlyStirred(1).mHat, ':k','linewidth',1.5, 'DisplayName', sprintf('p.s. %1.1f mL/min',data(2).FlowRate))

for i=1:length(data) -2
    plot(data(i).tHat_exp, data(i).mHat_exp, symbolsExp{i},'markersize', 5, 'DisplayName', sprintf('Exp. %1.1f mL/min, Re = %.1f$\\times 10^4$',data(i).FlowRate, data(i).ReynoldsNumber*1e-4))
    plot(data(i).tHat_sim, data(i).mHat_sim, symbolsSim{i},'linewidth',1.0, 'DisplayName', sprintf('RSM %1.1f mL/min, Re = %.1f$\\times 10^4$',data(i).FlowRate, data(i).ReynoldsNumber*1e-4))
end

% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
% xlabel('$t/(2 \pi \omega)$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$t$ [s]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$c_{out}/c_{in}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');

box on;

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized')

ytickformat('%,.1f')
% xtickformat('%,4.0f')
xtickformat('%,1.0f')
legend(gca, 'show', 'location', legendPos,'NumColumns',1)
legend('boxoff')

% xlim([0,8000])
xlim([0,2000])
ylim([0,0.45])
% xlim([0,300])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM500), 'pdf')
%saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor,RPM500),'-dpng','-r800');