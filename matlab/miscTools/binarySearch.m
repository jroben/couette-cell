function index = BinarySearch(sortedArray, target, searchType, verbose)
%Binary search of sorted Floating Point Array
%   type = 'exact' or 'closest' or 'firstBelow' or 'firstAbove'
%   verbose (optional) set to 'verbose' for output

%% Initialize variables.
if nargin<=3
    verbose = 'verbose';
end

n = length(sortedArray);
status = -999;
iter = 0;

leftBound = 0;
rightBound = n;
% -999: still searching; 0: found exact match; 1 found position;
% 2: less than min; 3 higher than max
% 4: UnsortedArray
% 5: Error
while (status < 0 && iter < n + 1)
    index = floor((rightBound + leftBound)/2);
    vLeft = sortedArray(index);
    vRight = sortedArray(index+1);
    cLeft = compareFloat(target, vLeft);
    cRight = compareFloat(target, vRight);
    if(cLeft == 0)
        status = 0;
    elseif(cRight == 0)
        index = index + 1; status = 0;
    elseif(cRight == 1 && index == n)
        index = -1; status = 3;
    elseif(cLeft == -1 && index == 1)
        index = -1; status = 2;
    elseif(cLeft == -1 && cRight == 1)
        
        if (strcmp(searchType, 'exact'))
            index = -1;
        elseif (strcmp(searchType, 'closest'))
            if(vRight-target < target-vLeft)
                index = index+1;
            end
        elseif (strcmp(searchType, 'firstAbove'))
            index = index + 1;
        end
        status = 1;
    elseif (cLeft == 1 && cRight == 1)
        rightBound = index;
    elseif (cLeft == -1 && cRight == -1)
        leftBound = index+1;
    else
        status = -4; index = -1;
    end
    iter = iter + 1;
    
end
if(strcmp(verbose, 'verbise'))
    fprintf('Array of length n=%i, took %i iterations, result: %i \n', n, iter, status);
end
end

function ind = compareFloat(x,y)
if(x==y)
    ind = 0;
elseif(x<y)
    ind = 1;
else
    ind = -1;
end
end