clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

Study = 'MeshConvergenceRST';
Rotor = 'Rotor1';
if strcmp(Rotor, 'Rotor1')
    RotorDiameter = 0.080;
    CellDiameter = 0.098;
    RotorLength = 0.138;
else
    RotorDiameter = 0.050;
    CellDiameter = 0.098;
    RotorLength = 0.138;
end
RPM = 1000;

density = 997.561;
dynamicViscosity = 8.8e-4;


kineticViscosity = dynamicViscosity/density;

baseSizesStr = {'5', '10', '15', '20', '30', '40'};



steadyIter = 20000;
nInnerIter = 5;
startPlotTime = 4e-3;
endPlotTime=0.02;
deltat = 0.5e-3;
samplingTime = 0.01; %s

StatorTorque        = zeros(length(baseSizesStr),1);
RotorTorque         = zeros(length(baseSizesStr),1);
MeanTorque          = zeros(length(baseSizesStr),1);
meanRotorTorque     = zeros(length(baseSizesStr),1);
meanStatorTorque    = zeros(length(baseSizesStr),1);
meanRotorOnlyTorque = zeros(length(baseSizesStr),1);
TorqueDiff          = zeros(length(baseSizesStr),1);
baseSizes           = zeros(length(baseSizesStr),1);

for i = 1:length(baseSizesStr)
    baseSizes(i) = str2double(baseSizesStr{i});

    res(i).baseSize = baseSizes(i);
    res(i).deltat = deltat * (res(i).baseSize/20) * ( 0.080/ RotorDiameter);
    res(i).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(i).deltat/10^(floor(log10(res(i).deltat))) ,floor(log10(res(i).deltat))  );
    
    fNameStringTime = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/PhysicalTime.csv', Study, Rotor, RPM, baseSizesStr{i});
    fNameStringRotor = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{i});
    fNameStringStator = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{i});
    fNameStringRotorOnly = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorOnlyMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{i});
    
    
    res(i).dfR = readTorque(fNameStringRotor);
    res(i).dfS = readTorque(fNameStringStator);
    res(i).dfT = readPhysicalTime(fNameStringTime);
    res(i).dfRO = readTorque(fNameStringRotorOnly);
    
    tsStart = ceil(startPlotTime / res(i).deltat);
    tsEnd = ceil(endPlotTime / res(i).deltat);
    
    res(i).iter = res(i).dfR.IterationIteration;
    res(i).itStart = find(res(i).iter == (steadyIter + tsStart * nInnerIter));
    res(i).itEnd = find(res(i).iter == (steadyIter + tsEnd * nInnerIter));
    
    res(i).timeIter = res(i).dfR.IterationIteration(res(i).itStart:res(i).itEnd);
    res(i).timelevel = (  res(i).timeIter - steadyIter ) / nInnerIter;  
    res(i).t =  res(i).timelevel * res(i).deltat;
    
    res(i).RotorToque = -res(i).dfR.Torque(res(i).itStart:res(i).itEnd);
    res(i).StatorTorque = res(i).dfS.Torque(res(i).itStart:res(i).itEnd);
    res(i).RotorOnlyToque = res(i).dfRO.Torque(res(i).itStart:res(i).itEnd);
    
    meanRotorTorque(i) = mean(res(i).RotorToque(end - floor(samplingTime/res(i).deltat )));
    meanStatorTorque(i) = mean(res(i).StatorTorque(end - floor(samplingTime/res(i).deltat )));
    meanRotorOnlyTorque(i) = mean(abs(res(i).RotorOnlyToque(end - floor(samplingTime/res(i).deltat ))));
    MeanTorque(i) = 0.5 * (meanRotorTorque(i)+meanStatorTorque(i));
    TorqueDiff(i) = meanRotorTorque(i)-meanStatorTorque(i);
end


legendPos = 'sw';
figureName = 'b';

paperfactorX = 0.55;
paperfactorY = 0.55*1.0;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

T = meanRotorOnlyTorque(1);
G = T/(density* kineticViscosity.^2 *(RotorLength));
fprintf('RPM = %i\n',RPM)
fprintf('T = %8.4e\n',T)
fprintf('G = %8.4e\n',G)

% plot(log10(baseSizes/100), MeanTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'Mean Torque (Stator, Rotor)')
% hold on
semilogx(baseSizes/100, meanRotorOnlyTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'RST-LES')
hold on
% plot(log10(baseSizes/100), meanStatorTorque * 1000, ':xr', 'MarkerSize', markerSize, 'displayname', 'Mean Stator Torque')
% hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.2f')
%legend(gca, 'show', 'location', legendPos)
% ylim([7,12])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')

return
%% Time series
hold off;
paperfactorX = 0.55*2;
paperfactorY = 0.55*1.;
markerSize = 3;
legendPos = 'se';

f2 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f2, 'PaperUnits', 'centimeters');
set(f2, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f2, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbols = {'m', 'b', 'y', 'c', 'k', 'r', 'g'};


for i = 1:length(res)
    plot(res(i).t, 1000*(res(i).RotorToque), sprintf('-%s',symbols{i}), ...
        'displayname', sprintf('$ \\hat{\\delta}  = %1.3f$', res(i).baseSize*0.01),...
        'MarkerSize', markerSize );
    hold on

    %plot(res(i).t, 1000*(res(i).StatorTorque), sprintf(':%s',symbols{i}), ...
    %    'displayname', sprintf('$ \\hat{\\delta}  = %1.3f$', res(i).baseSize*0.01),...
    %    'MarkerSize', markerSize );

end

ylim auto

text(0.85,0.95,sprintf('\\textbf{(%c)}','b'),'fontsize',12,...
    'Units', 'normalized')
xlabel('t [s]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)

saveas(f2, sprintf('Output/MeshTimeSeries_%s_%iRPM_',Rotor, RPM), 'pdf')
saveas(f2, sprintf('Output/MeshTimeSeries_%s_%iRPM_',Rotor, RPM), 'fig')
%% RotorOnly
return
legendPos = 'sw';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(log10(baseSizes), meanRotorOnlyTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'Mean Torque')
hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}','a'),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)
% ylim([7,12])
xlim([0 max(log10(baseSizes)+0.1)])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',strcat(Study, 'RotorOnly'),Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',strcat(Study, 'RotorOnly'),Rotor,RPM), 'fig')