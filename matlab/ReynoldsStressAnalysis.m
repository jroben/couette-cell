clear all; clc; close all;


data = importReynoldsStressesAndVelocityGradient('./Data/RSTandGradU.csv');


n = length(data.Xm);
averageDiff = zeros(3,3);
averageDiffFlux

for i = 1: length(n)
    
    res(i).RST = [...
        data.ReynoldsStressTensorii(i),data.ReynoldsStressTensorij(i),data.ReynoldsStressTensorik(i);...
        data.ReynoldsStressTensorij(i),data.ReynoldsStressTensorjj(i),data.ReynoldsStressTensorjk(i);...
        data.ReynoldsStressTensorik(i),data.ReynoldsStressTensorjk(i),data.ReynoldsStressTensorkk(i)...
        ];
    
    res(i).gradU = [...
        data.VelocityGradientTensorii(i),data.VelocityGradientTensorij(i),data.VelocityGradientTensorik(i);...
        data.VelocityGradientTensorij(i),data.VelocityGradientTensorjj(i),data.VelocityGradientTensorjk(i);...
        data.VelocityGradientTensorik(i),data.VelocityGradientTensorjk(i),data.VelocityGradientTensorkk(i)...
        ];

    res(i).nuTensorDiff = (norm(res(i).RST,2) ./ norm(res(i).gradU,2))*res(i).gradU - res(i).RST;
    averageDiff = averageDiff + 1/n * res(i).nuTensorDiff;
end