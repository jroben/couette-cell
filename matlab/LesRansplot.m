clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

Study = 'LesRansComparison';
Studies = {'MeshConvergence7','MeshConvergence10'};
Rotor = 'Rotor1';
if strcmp(Rotor, 'Rotor1')
    RotorD = 0.080;
else
    RotorD = 0.050;
end
RPM = 500;

baseSizesStr = {'10','5'};
leg = {'RANS', 'LES'};


steadyIter = 20000;
nInnerIter = 5;
startPlotTime = 4e-3;
deltat = 0.5e-3;
samplingTime = 0.5; %s

StatorTorque        = zeros(length(baseSizesStr),1);
RotorTorque         = zeros(length(baseSizesStr),1);
MeanTorque          = zeros(length(baseSizesStr),1);
meanRotorTorque     = zeros(length(baseSizesStr),1);
meanStatorTorque    = zeros(length(baseSizesStr),1);
meanRotorOnlyTorque = zeros(length(baseSizesStr),1);
TorqueDiff          = zeros(length(baseSizesStr),1);
baseSizes           = zeros(length(baseSizesStr),1);

for i = 1:length(Studies)
    baseSizes(i) = str2double(baseSizesStr{i});

    res(i).baseSize = baseSizes(i);
    res(i).deltat = deltat * (res(i).baseSize/20) * ( 0.080/ RotorD);
    res(i).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(i).deltat/10^(floor(log10(res(i).deltat))) ,floor(log10(res(i).deltat))  );
    
    fNameStringRotor = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Studies{i}, Rotor, RPM, baseSizesStr{i});
    fNameStringStator = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Studies{i}, Rotor, RPM, baseSizesStr{i});
    fNameStringRotorOnly = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorOnlyMomentMonitor.csv', Studies{i}, Rotor, RPM, baseSizesStr{i});
    
    res(i).dfR = readTorque(fNameStringRotor);
    res(i).dfS = readTorque(fNameStringStator);
   % res(i).dfRO = readTorque(fNameStringRotorOnly);
    
    tsStart = ceil(startPlotTime / res(i).deltat);
    
    res(i).iter = res(i).dfR.IterationIteration;
    res(i).itStart = find(res(i).iter == (steadyIter + tsStart * nInnerIter));
    
    res(i).timeIter = res(i).dfR.IterationIteration(res(i).itStart:end);
    res(i).timelevel = (  res(i).timeIter - steadyIter ) / nInnerIter;  
    res(i).t =  res(i).timelevel * res(i).deltat;
    
    res(i).RotorToque = -res(i).dfR.Torque(res(i).itStart:end);
    res(i).StatorTorque = res(i).dfS.Torque(res(i).itStart:end);
    %res(i).RotorOnlyToque = res(i).dfRO.Torque(res(i).itStart:end);
    
    meanRotorTorque(i) = mean(res(i).RotorToque(end - floor(samplingTime/res(i).deltat )));
    meanStatorTorque(i) = mean(res(i).StatorTorque(end - floor(samplingTime/res(i).deltat )));
    %meanRotorOnlyTorque(i) = mean(abs(res(i).RotorOnlyToque(end - floor(samplingTime/res(i).deltat ))));
    MeanTorque(i) = 0.5 * (meanRotorTorque(i)+meanStatorTorque(i));
    TorqueDiff(i) = meanRotorTorque(i)-meanStatorTorque(i);
end

%% plotting


legendPos = 'sw';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = 0;%-0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(log10(baseSizes), MeanTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'Mean Torque (Stator, Rotor)')
hold on
plot(log10(baseSizes), meanRotorTorque * 1000, ':xb', 'MarkerSize', markerSize, 'displayname', 'Mean Rotor Torque')
hold on
plot(log10(baseSizes), meanStatorTorque * 1000, ':xr', 'MarkerSize', markerSize, 'displayname', 'Mean Stator Torque')
hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}','a'),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)
% ylim([7,12])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')


%% Time series
hold off;
paperfactorX = 0.55*2;
paperfactorY = 0.55*1.3;
markerSize = 3;
legendPos = 'se';

fac1 = 0.01;%0.01;
fac2 = 0.01;
fac3 = 0.99+0.03;
fac4 = 0.99;

f2 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f2, 'PaperUnits', 'centimeters');
set(f2, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f2, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbols = {'m', 'b', 'y', 'c', 'k', 'r', 'g'};

res(1).RotorToque = res(1).RotorToque * (0.0118 / res(1).RotorToque(end));
res(2).RotorToque = res(2).RotorToque * (0.0118 / res(2).RotorToque(end));

res(1).t = res(1).t * (5/res(1).t(end));
res(2).t = res(2).t * (5/res(2).t(end));
for i = 1:length(res)
    plot(res(i).t, 1000*(res(i).RotorToque), sprintf('-%s',symbols{i}), ...
        'displayname', leg{i},...
        'MarkerSize', markerSize );
    hold on

    %plot(res(i).t, 1000*(res(i).StatorTorque), sprintf(':%s',symbols{i}), ...
    %    'displayname', sprintf('$ \\hat{\\delta}  = %1.3f$', res(i).baseSize*0.01),...
    %    'MarkerSize', markerSize );

end

ylim auto

text(0.85,0.95,sprintf('\\textbf{(%c)}','b'),'fontsize',12,...
    'Units', 'normalized')
xlabel('t [s]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)

saveas(f2, sprintf('Output/MeshTimeSeries_%s_%iRPM_',Rotor, RPM), 'pdf')
saveas(f2, sprintf('Output/MeshTimeSeries_%s_%iRPM_',Rotor, RPM), 'fig')
%% RotorOnly
legendPos = 'sw';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(log10(baseSizes), meanRotorOnlyTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'Mean Torque')
hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}','a'),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)
% ylim([7,12])
xlim([0 max(log10(baseSizes)+0.1)])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',strcat(Study, 'RotorOnly'),Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',strcat(Study, 'RotorOnly'),Rotor,RPM), 'fig')