clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);


Rotor = 'Rotor1';
RPM = 500;
deltat = {'0.5e-3','1e-3','2e-3', '4e-3'};
deltats = [0.5e-3, 1e-3,2e-3,4e-3];

steadyIter = 1500;
nInnerIter = 5;
startPlotTime = 4e-3;

meanStatorTorque = zeros(length(deltat),1);
meanRotorTorque = zeros(length(deltat),1);
MeanTorque = zeros(length(deltat),1);
TorqueDiff = zeros(length(deltat),1);
baseSizes = zeros(length(deltat),1);

res = {};

for i = 1:length(deltat)
    %res(i) = {};
    res(i).deltat = deltats(i);
    res(i).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(i).deltat/10^(floor(log10(res(i).deltat))) ,floor(log10(res(i).deltat))  );
    
    fNameStringRotor = sprintf('./Data/TimeStepConvergence/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Rotor, RPM, deltat{i});
    fNameStringStator = sprintf('./Data/TimeStepConvergence/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Rotor, RPM, deltat{i});
    
    res(i).dfR = readTorque(fNameStringRotor);
    res(i).dfS = readTorque(fNameStringStator);
    
    tsStart = ceil(startPlotTime / res(i).deltat);
    
    res(i).iter = res(i).dfR.IterationIteration;
    res(i).itStart = find(res(i).iter == (steadyIter + tsStart * nInnerIter));
    
    res(i).timeIter = res(i).dfR.IterationIteration(res(i).itStart:end);
    res(i).timelevel = (  res(i).timeIter - steadyIter ) / nInnerIter;  
    res(i).t =  res(i).timelevel * res(i).deltat;
    
    res(i).RotorToque = -res(i).dfR.Torque(res(i).itStart:end);
    res(i).StatorTorque = res(i).dfS.Torque(res(i).itStart:end);
    
    meanRotorTorque(i) = mean(res(i).RotorToque);
    meanStatorTorque(i) = mean(res(i).StatorTorque);
    MeanTorque(i) = 0.5 * (meanRotorTorque(i)+meanStatorTorque(i));
    TorqueDiff(i) = meanRotorTorque(i)-meanStatorTorque(i);
end


%legendPos = 'sw';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.05;
markerSize = 3;

%% Convergence
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(log10(deltats), MeanTorque * 1000, 'xk', 'MarkerSize', markerSize)
hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}','a'),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')

saveas(f1, sprintf('Output/DeltatConvergence_%s_%iRPM_',Rotor,RPM), 'pdf')


%% Time series
hold off;
paperfactorX = 0.55*2;
paperfactorY = 0.55*1.3;
markerSize = 3;
legendPos = 'se';

f2 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f2, 'PaperUnits', 'centimeters');
set(f2, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f2, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbols = {'dr', '^b', '*y', 'cx'};


for i = 1:length(res)
    plot(res(i).t, 500*(res(i).RotorToque + res(i).StatorTorque), symbols{i}, ...
        'displayname', sprintf('$ \\Delta t = %s$', res(i).deltatLegendName),...
        'MarkerSize', markerSize );
hold on
end
text(0.85,0.95,sprintf('\\textbf{(%c)}','b'),'fontsize',12,...
    'Units', 'normalized')
xlabel('t [s]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')

legend(gca, 'show', 'location', legendPos)
saveas(f2, sprintf('Output/DeltatTimeSeries_%s_%iRPM_',Rotor, RPM), 'pdf')
