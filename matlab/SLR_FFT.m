function [f, P] = SLR_FFT(t,y,varargin)
% clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A wrapper function for MatLab's FFT-function.
% Building on SohFFT by Teis Schnipper.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% An even number of data is needed.
if mod(length(t),2) 
    t = t(1:end-1);
    y = y(1:end-1);
end

if size(y,1)>size(y,2)
    y = y';
end

if size(t,1)>size(t,2)
    t = t';
end

% As default, block length is set to signal length
Nb = length(t); % block length = signal length

% Default to no overlap
overlapFactor   = 0;
overlap         = 0;

% Default to no windowing, but default window method is Welch
windowing       = 0;
windowMethod    = 'Welch';

% Default to no linear regression
linearDetrend       = 0;

% Read input arguments, if any
% disp(varargin)
if ~isempty(varargin)
    
    i = 1;
    while i<length(varargin)
        switch varargin{i}

            % BLOCKLENGTH
            case 'BlockLength'
                Nb = varargin{i+1};
                disp(['BlockLength set to ' num2str(Nb) ' samples.'])
                
                if Nb > length(t)
                    Nb = length(t);
                    disp('BlockLength too large, set to signal length.')
                end
            
            % OVERLAP
            case 'Overlap'
                overlapFactor = varargin{i+1};
                disp(['Overlap Factor set to ' num2str(overlapFactor) '.'])
                if Nb < length(t) && overlapFactor ~= 0
                    overlap = 1; % Switches overlap on
                    disp('Overlap enabled.')
                else 
                    disp('Overlap not enabled. Block Length must be smaller than signal length.')
                end
                
            % WINDOWING
            case 'Windowing'
                windowing = 1;
                disp('Windowing enabled.')
                
            % WINDOW METHOD
            case 'WindowMethod'
                windowMethod = varargin{i+1};
                disp(['Windowing method set to: ' windowMethod '.'])
                
%             otherwise
%                 disp(['Variable input argument #' num2str(i) ' not recognized.'])

            % LINEAR REGRESSION
            case 'LinDetrend'
                linearDetrend = 1;
                disp('Linear Detrending per block has been enabled.');
                
        end
        
        i = i + 1;
    end 
end

% Window function
switch windowMethod
    case 'Welch'
        W =@(j) 1 - ((j - (1/2)*Nb)/((1/2)*Nb)).^2;
        
    case 'Bartlett'
        W =@(j) 1 - abs((j - (1/2)*Nb)/((1/2)*Nb));
        
    case 'Hann'
        W =@(j) (1/2)*(1 - cos((2*pi*j)/Nb));
        
end
for i=1:Nb
    Wi(i) = W(i);
end
W_SS = Nb * sum(Wi.^2);

% Determining overlap length
overlapLength = 0;
if overlap
    overlapLength = Nb*overlapFactor;
end

% prepare
N    = length(t);                 % signal length, for frequency
M    = floor(N/(Nb-overlapLength));             % number of blocks

Fs   = (N-1)/(t(end)-t(1));       % (Average) sample frequency
f    = Fs/2*linspace(0,1,Nb/2+1); % frequency vector

if Nb>N
    error('Block length larger than signal length.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%% BLOCK AVERAGED %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FFT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Do (block averaged) FFT
for k=1:M % loop over blocks

    % Extract block data
    if  N - ((k-1)*(Nb-overlapLength)+1) < Nb && M > 1
        break
    end
    if k == 1
        yt = y( 1 : Nb );
    elseif k == M
        if length(y( ((k-1)*(Nb-overlapLength)+1) : end))<Nb
            break
        end
        yt = y( ((k-1)*(Nb-overlapLength)+1) : k*Nb - (k-1)*overlapLength);
    else
        yt = y( ((k-1)*(Nb-overlapLength)+1) : k*Nb - (k-1)*overlapLength);
    end
    
    % Apply linear detrending
    if linearDetrend
%         xs = linspace(1,Nb,Nb);
%         p = polyfit(xs, yt, 1);
%         pVal = polyval(p,xs);
%         yt = yt - pVal;
        yt = detrend(yt);
    end
    
    % Apply window function
    if windowing
        yt = yt.*Wi;
    end
    
    % Fourier transformation
    Yt       = fft(yt); 

    % Generate power spectrum
    Pt2(:,k) = abs(Yt(1:Nb/2+1)).^2;
end

% Perform block average over power spectra
P(:) = mean(Pt2,2);

% Normalize windowed spectrum
if windowing
    P = (P*Nb^2)/W_SS;
end
% Normalize non-windowed spectrum
    P = (P)/(Fs*Nb); % bring out here, for speed.

P(2:end-1)   = 2*P(2:end-1); % only consider DC and Nyquist frequency once!

end
