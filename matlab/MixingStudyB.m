clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'MixingStudyB';
Rotor = 'Rotor1';


RotorDiameter = 0.080;

RPM = [500, 1000];
FlowRatesString = {'2xFlow'};
FlowRates = [16.6];


% RPM = 1000;
% FlowRatesString = {'2xFlow'};
% FlowRates = [16.6];

for i = 1:length(RPM)
    dataFilesSim{i} = sprintf('./Data/Mixing/%iRPM/%sMassFlowAveragedOutletConcentrationofSodiumMonitor.csv', RPM(i), FlowRatesString{1});
    dataFilesExp{i} = sprintf('./Data/Mixing/%iRPM/Experiment%s.csv',RPM(i),FlowRatesString{1});
end


Concentration = 0.0119146015602731;

calConcentration = 1e-4;
data = {};

Omega = RPM/60*2*pi;

%% Extract Data
for i = 1:length(dataFilesSim)
    data(i).Simulations = importOutflowConcentration(dataFilesSim{i});
    
    data(i).Experimental = importExperimentalConcentrations(dataFilesExp{i});
    data(i).FlowRate = FlowRates(1);
    data(i).RPM = RPM(i);
end




%% Transform
for i = 1:length(data)
     
    t_cal_index = min(find(data(i).Experimental.CeffICPmolkg>calConcentration));
    t_cal = data(i).Experimental.ts(t_cal_index);
    m_cal = data(i).Experimental.CeffICPmolkg(t_cal_index);
    
    t_cal_sim_index = min(find(data(i).Simulations.MassFlowAveragedOutletConcentrationofSodiumMonitorMassFlowAvera > m_cal));
    t_cal_sim = data(i).Simulations.PhysicalTimePhysicalTimes(t_cal_sim_index);
    
    data(i).t_sim = data(i).Simulations.PhysicalTimePhysicalTimes(t_cal_sim_index:end) - t_cal_sim;
    data(i).m_sim = data(i).Simulations.MassFlowAveragedOutletConcentrationofSodiumMonitorMassFlowAvera(t_cal_sim_index:end);
    
    data(i).tHat_sim = data(i).t_sim*(RPM(i)/60);
    data(i).mHat_sim = data(i).m_sim/(Concentration/2);
    

    data(i).t_exp = data(i).Experimental.ts(t_cal_index:end) - t_cal;
    data(i).m_exp = data(i).Experimental.CeffICPmolkg(t_cal_index:end);
    
    data(i).tHat_exp = data(i).t_exp*(RPM(i)/60);
    data(i).mHat_exp = data(i).m_exp/(Concentration/2);
end


%% Plot
legendPos = 'no';
figureName = 'b';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbolsExp = {'xr', '+b'};
symbolsSim = {'-r', '--b'};
for i=1:length(data)
    plot(data(i).tHat_exp, data(i).mHat_exp, symbolsExp{i}, 'DisplayName', sprintf('Exp. %i RPM',data(i).RPM))
    hold on
    plot(data(i).tHat_sim, data(i).mHat_sim, symbolsSim{i}, 'DisplayName', sprintf('RST %i RPM',data(i).RPM))
end

% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$t/(2 \pi \omega)$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$c_{out}/c_{in}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized')

ytickformat('%,.3f')
xtickformat('%,4.0f')
legend(gca, 'show', 'location', legendPos,'NumColumns',2)
legend('boxoff')

xlim([0,3000])

saveas(f1, sprintf('Output/%s_%s_',Study,Rotor), 'pdf')
saveas(f1, sprintf('Output/%s_%s_',Study,Rotor), 'fig')
print(f1,sprintf('Output/%s_%s.png',Study,Rotor),'-dpng','-r800');