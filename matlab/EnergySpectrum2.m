clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'EnergySpectrum2';
Rotor = 'Rotor1';
RPM = 1000;
dt = 1e-2;
zMin = 0;
zMax = 0.138;

%% Data
t = 0;
Files=dir(sprintf('./Data/Velocities/%iRPM/output/*.csv',RPM));
for i=1:length(Files)
   res(i).data = importVelocityExportXZ(strcat(Files(i).folder,'/',Files(i).name));
   res(i).t = t;
   t = t + dt;
end

%%
nPointsTotal = length(res(1).data.CentroidZm);
indices = find(res(1).data.CentroidZm > zMin .* res(1).data.CentroidZm < zMax);

indices = indices(1:1000:end); %sampling
nPoints = length(indices);
nTimesteps = length(res);
point_fft = [];
for i=1:nPoints
    y = zeros(1, nTimesteps);
    ind = indices(i);
    for j = 1:nTimesteps
        u(j) = res(j).data.Velocityims(ind);
        v(j) = res(j).data.Velocityjms(ind);
        w(j) = res(j).data.Velocitykms(ind);
        t(j) = res(j).t;
    end
    
    [point_fft(i).fftu_k, point_fft(i).fftu_E] = SLR_FFT(t,u);
    [point_fft(i).fftv_k, point_fft(i).fftv_E] = SLR_FFT(t,v);
    [point_fft(i).fftw_k, point_fft(i).fftw_E] = SLR_FFT(t,w);
    
end



%% plot
for i=1:nPoints
    loglog(point_fft(i).fftw_k, point_fft(i).fftw_E, '.')
    hold on
end

%%
return
k1 = fftx.Wavenumbersradm(2);
kn = max(fftx.Wavenumbersradm);

k53LineX = [k1 kn];
k53LineLogX = log10(k53LineX);

Emax = max(max([fftx.LineDerivedPowerSpectralDensityPa2Hzm3s ffty.LineDerivedPowerSpectralDensityPa2Hzm3s fftz.LineDerivedPowerSpectralDensityPa2Hzm3s]));

logEmax = log10(Emax);

k53LineLogY = [logEmax+0.1 (logEmax+0.1)-(k53LineLogX(2)-k53LineLogX(1))*(5/3)];
k53LineY = 10.^(k53LineLogY);


%% plot
legendPos = 'no';
figureName = 'b';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

loglog(fftx.Wavenumbersradm,fftx.LineDerivedPowerSpectralDensityPa2Hzm3s, '--r', 'DisplayName', '$u_x$')
hold on
loglog(ffty.Wavenumbersradm,ffty.LineDerivedPowerSpectralDensityPa2Hzm3s, ':b', 'DisplayName', '$u_y$')
loglog(fftz.Wavenumbersradm,fftz.LineDerivedPowerSpectralDensityPa2Hzm3s, '-g', 'DisplayName', '$u_z$')
loglog(k53LineX, k53LineY, '-k', 'DisplayName', '$k^5/3$')
% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$E$ [m$^2$/s]', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$k$', 'FontSize', 12, 'FontWeight', 'bold');

ytickformat('%,.1f')
xtickformat('%,.0f')
legend(gca, 'show', 'location', legendPos,'NumColumns',1)
legend('boxoff')

saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor,RPM),'-dpng','-r800');