clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'Reactive1';
Rotor = 'Rotor1';


if strcmp(Rotor, 'Rotor1')
    RotorDiameter = 0.080;
    CellDiameter = 0.098;
    RotorLength = 0.138;
    CellVolume=0.3627951/997.561;
else
    RotorDiameter = 0.050;
    CellDiameter = 0.098;
    RotorLength = 0.138;
end

density = 997.561;
dynamicViscosity = 8.82E-07*density;
kineticViscosity = dynamicViscosity/density;


RPM500 = 500;
% FlowRatesString = {'1xFlow','2xFlow'};
FlowRates500 = [8.3, 8.3];
%FlowRates = [8.3];

ResidenceTime= CellVolume./(FlowRates500/(60*1e6));

RPMs = [500, 1000];


%model = 'LES2'; %LES / RST
%ion = 'mNa_1+'; %mNa_1+ /mSO4_2-
% RPM = 1000;
% FlowRatesString = {'2xFlow'};
% FlowRates = [16.6];

for i = 1:length(RPMs)
%     dataFilesSim{i} = sprintf('./Data/Reactive/Effluent/%s/%iRPM/%1.1fml/MassFlowAveragedOutletConcentrationof%sMonitor.csv',model, RPM500, FlowRates500(i), ion);
    dataFilesExp{i} = sprintf('./Data/Reactive/Effluent/%s/%iRPM/Exp.csv',Rotor,RPMs(i));
end



ExpConcentration = 8.3588E-04; % 2

% calConcentration = 0.1;
data = {};

%% Extract Data
for i = 1:length(RPMs)
%     data(i).Simulations = importOutflowConcentration3(dataFilesSim{i});
    
    data(i).Experimental = importExperimentalConcentrations2(dataFilesExp{i});
    
    data(i).FlowRate = FlowRates500(i);
    data(i).RPM = RPMs(i);
    Omega = data(i).RPM/60*2*pi;
    data(i).ReynoldsNumber = Omega*0.5*RotorDiameter*(0.5*(CellDiameter-RotorDiameter))/kineticViscosity;
    
%     
%     data(i).Sim.tHat = data(i).Simulations.Time / ResidenceTime(i) ;
%     data(i).Sim.mHat = data(i).Simulations.Concentration / SimConcentration(i);
    
    data(i).Exp.tHat=data(i).Experimental.Time / ResidenceTime(i);
    data(i).Exp.mHat=data(i).Experimental.Concentration / ExpConcentration;
    
end


%% Transform
for i = 1:length(data)
    
    data(i).tHat_exp = data(i).Exp.tHat(1:end) ;
    data(i).mHat_exp = data(i).Exp.mHat(1:end) ;
    
end


%% Plot
legendPos = 'no';
figureName = 'a';

% paperfactorX = 0.55;
% paperfactorY = 0.55*1.3;

paperfactorX = 0.55 * 2;
paperfactorY = 0.55;

marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbolsExp = {'xr', '+b', '*g' , 'om'};
symbolsSim = {'-r', '--b'};

hold on

for i=1:length(data)
    plot(data(i).tHat_exp, data(i).mHat_exp, symbolsExp{i},'markersize', 3, 'DisplayName', sprintf('Exp. %1.1f mL/min, Re = %.1fx$10^4$',data(i).FlowRate, data(i).ReynoldsNumber*1e-4))
end
% plot(data(1).tHat_sim, data(1).mHat_sim, symbolsSim{1}, 'DisplayName', sprintf('RST %1.1f mL/min, Re = %.1fx$10^4$',data(1).FlowRate, data(1).ReynoldsNumber*1e-4))
% plot(data(2).tHat_sim, data(2).mHat_sim, symbolsSim{2}, 'DisplayName', sprintf('RST %1.1f mL/min, Re = %.1fx$10^4$',data(2).FlowRate, data(2).ReynoldsNumber*1e-4))



% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
% xlabel('$t/(2 \pi \omega)$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$t/t_{res}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$c_{out}/c_{in}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');

% text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12, 'Units', 'normalized')

ytickformat('%,.3f')
% xtickformat('%,4.0f')
xtickformat('%,1.2f') 
legend(gca, 'show', 'location', legendPos,'NumColumns',1)
legend('boxoff')

% xlim([0,8000])
xlim([0,1])
 ylim([0,0.05])
% xlim([0,300])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM500), 'pdf')
%saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor,RPM500),'-dpng','-r800');