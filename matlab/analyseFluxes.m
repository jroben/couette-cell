clear all; clc; close all
files = {'Rough_Re1034', 'Rough_Re4808', 'Smooth_Re1034', 'Smooth_Re4808'};


for i=1:length(files)
    data(i).data = importBoundaryFlux(sprintf('./Data/Extracts/%s.csv',files{i}));
    data(i).name = files{i};
    
    
end

legendPos = 'sw';
figureName = 'b';

paperfactorX = 0.55;
paperfactorY = 0.55*1.0;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

for i=1:length(files)
    hold on;
    plot(data(i).data.Ym/0.03, data(i).data.Flux,'x', 'displayname', data(i).name)
    %plot(data(i).data.Ym, data(i).data.Zm,'-')
end

xlabel('$y/L$ [-]')
ylabel('Flux/m$_{inlet}$ [m$^{-2}$ s$^{-1}$]')
ytickformat('%,.1f')
xtickformat('%,.2f')
legend(gca, 'show', 'location', legendPos)