clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Input
Study = 'EskinComparison';
Rotor = 'Rotor1';
if strcmp(Rotor, 'Rotor1')
    RotorDiameter = 0.080;
    CellDiameter = 0.098;
    RotorLength = 0.138;
else
    RotorDiameter = 0.050;
    CellDiameter = 0.098;
    RotorLength = 0.138;
end
    
density = 997.561;
dynamicViscosity = 8.82e-7*density;

RPM = 50:50:1500;
flow = 8.3; % flow rate in ml


%% Sim results

% 250 RPM
RPM_simulated(1)=250;
T_LES(1)=3.002e-03;
T_RST(1)=3.002e-03;
G_simulated(1)=8.8206e+07;

% 500 RPM
RPM_simulated(2)=500;
T_LES(2)=9.348e-03;
T_RST(2)=9.4460e-02;
G_simulated(2)=8.8206e+07;

% 750 RPM
RPM_simulated(3)=750;
T_LES(3)=1.8580e-02;
T_RST(3)=1.8740e-02;
G_simulated(3)=1.7493e+08;


% 1000 RPM
RPM_simulated(4)=1000;
T_LES(4)=3.0130e-02;
T_RST(4)=3.0480e-02; 
G_simulated(4)=2.8471e+08;



%% Conversions
RotorRadius = (RotorDiameter/2); % inner radius in m
CellRadius = (CellDiameter/2); % outer radius in m

eta = (RotorRadius/CellRadius); %radius ratio
aspect_ratio = RotorLength/(CellRadius-RotorRadius);
volume_annulus = pi()*RotorLength*(CellRadius^2-RotorRadius^2);

%conversion from m3 to ML
volume_1_ml = volume_annulus * 10^6;

%calculation of residence time in Couette Cell
tau_1 = volume_1_ml./flow;

kineticViscosity = dynamicViscosity/density; % kinematic viscosity
mass = density * volume_annulus;
% eskin G parameters
kappa = 0.44;
xi = 0.406;
phi = (2/(1/eta + eta)) - log (1+eta/ (1-eta));
fun1= (kappa/(1/eta +eta))* ((2*pi())^0.5)/(1-eta);


for i = 1: length(RPM_simulated)
    Omega_simulated = 2*RPM_simulated(i)* pi()/60;
    Reynolds_simulated(i) = Omega_simulated * RotorRadius*(CellRadius-RotorRadius)/kineticViscosity;
end

Omega = 2*RPM* pi()/60;
Re = Omega * RotorRadius*(CellRadius-RotorRadius)/kineticViscosity;


%% Solve Eskin's equation for G
% preallocation of arrays
G_lathrop = 0.23*eta^(2/3)/(1-eta)^(7/4)*Re.^(1.7);
T_lathrop =  density* kineticViscosity(1).^2 *(RotorLength)* G_lathrop;



G_eskin= zeros (1, length(RPM));
T_eskin = zeros(1, length(RPM));
shear_stress_eskin = zeros (1, length(RPM));


for i = 1: length(RPM)
    syms g
    
%     G_eskin (i) = double((solve(fun1.*Re(i) == sqrt(g)*(log(sqrt(g)) + phi + xi), g)));
    T_eskin(i) = density* kineticViscosity(1).^2 *(RotorLength)* G_eskin (i);
    shear_stress_eskin(i) = T_eskin(i)/(2*pi()*RotorLength*CellRadius^2); 
    
end

%% plot

legendPos = 'no';
figureName = 'b';

paperfactorX = 0.55*1.25;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',8,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

% plot (RPM, T_eskin*1000,':b','displayname','[Eskin, 2010]','LineWidth', 2 )
hold on
plot (Re, T_lathrop,':r','displayname',sprintf('[Wendt, 1933]\n'),'LineWidth', 2 )

plot(Reynolds_simulated, T_LES, 'ok','displayname', 'LES','MarkerSize', markerSize )
plot(Reynolds_simulated, T_LES, 'xk','displayname', 'RSM','MarkerSize', markerSize )

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized', 'FontName', 'Times New Roman')

ax = gca;
ax.YAxis.Exponent = -3;
xlim([0 50e3])
 ylim([0.000,0.050])
xlabel ('Reynolds Number')
ylabel('Torque [Nm]')
ytickformat('%,.0f')
xtickformat('%,.0f')
legend(gca, 'show', 'location', legendPos,'NumColumns',1)
legend('boxoff')
set(gca, 'FontName', 'Times New Roman')
box on

saveas(f1, sprintf('Output/%s_%s_%iRPM',Study,Rotor), 'pdf')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor),'-dpng','-r800');         
saveas(f1, sprintf('Output/%s_%s_%iRPM',Study,Rotor), 'fig')
