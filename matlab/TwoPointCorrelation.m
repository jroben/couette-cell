% clear all;
clc; close all;

Files=dir('Data/lesCourse/Table_SC2/*.*');
n = length(Files)-2;

i_p = 50;


% 
% for k=3:length(Files)
%    FileName = Files(k).name;
%    FilePAth = Files(k).folder;
%    fullPath = strcat(FilePAth, '/', FileName);
%    data(k-2).data = importLineProbeVelocities(fullPath);
%    data(k-2).t = (k-2)*1e-3;
%    data(k-2).z_p = data(k-2).data.Zm(i_p);
%    data(k-2).u_p = data(k-2).data.Velocityims(i_p);
%    
%    t(k-2) = data(k-2).t;
%    u_p(k-2) = data(k-2).u_p;
% end


zp = data(1).z_p;
np = length(data(1).data.Velocityims);

for i = 1:np
    corr(i) = 0;
    dz(i) = zp - data(1).data.Zm(i);
    for j = 1:n 
        up(j) = data(j).data.Velocityims(i_p);
        uj(j) = data(j).data.Velocityims(i);
    end
    up = up - mean(up);
    uj = uj - mean(uj);
    
    for j = 1:n
        corr(i) = corr(i) + (1/n)*(up(j)*uj(j));
    end
end

[dz_s ,idz] = sort(dz);
corr_s = corr(idz)

plot(dz_s/0.01,corr_s, '-')
xlabel('$r/\Delta z$', 'interpreter', 'latex')
ylabel('$u_1(z) u_1(z-r)$', 'interpreter', 'latex')