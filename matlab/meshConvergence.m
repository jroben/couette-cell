clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

Study = 'MeshConvergence7';
Rotor = 'Rotor1';
RPM = 500;
baseSizesStr = {'3.125','5','6.25','10','12.5','20'};
%baseSizesStr = {'10','20', '40'};
baseSizesStr = {'7.5','10','15','20','30','40'};
baseSizesStr = {'10','15','20','30','40'};


steadyIter = 30000;
nInnerIter = 5;
startPlotTime = 4e-3;
deltat = 1e-3;
samplingTime = 0.5; %s

StatorTorque = zeros(length(baseSizesStr),1);
RotorTorque = zeros(length(baseSizesStr),1);
MeanTorque = zeros(length(baseSizesStr),1);
meanRotorTorque  = zeros(length(baseSizesStr),1);
meanStatorTorque  = zeros(length(baseSizesStr),1);
TorqueDiff = zeros(length(baseSizesStr),1);
baseSizes = zeros(length(baseSizesStr),1);

for i = 1:length(baseSizesStr)
    baseSizes(i) = str2double(baseSizesStr{i});
    %res(i) = {};
    res(i).baseSize = baseSizes(i);
    res(i).deltat = deltat;
    res(i).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(i).deltat/10^(floor(log10(res(i).deltat))) ,floor(log10(res(i).deltat))  );
    
    fNameStringRotor = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{i});
    fNameStringStator = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{i});
    
    res(i).dfR = readTorque(fNameStringRotor);
    res(i).dfS = readTorque(fNameStringStator);
    
    tsStart = ceil(startPlotTime / res(i).deltat);
    
    res(i).iter = res(i).dfR.IterationIteration;
    res(i).itStart = find(res(i).iter == (steadyIter + tsStart * nInnerIter));
    
    res(i).timeIter = res(i).dfR.IterationIteration(res(i).itStart:end);
    res(i).timelevel = (  res(i).timeIter - steadyIter ) / nInnerIter;  
    res(i).t =  res(i).timelevel * res(i).deltat;
    
    res(i).RotorToque = -res(i).dfR.Torque(res(i).itStart:end);
    res(i).StatorTorque = res(i).dfS.Torque(res(i).itStart:end);
    
    meanRotorTorque(i) = mean(res(i).RotorToque(end - samplingTime/res(i).deltat ));
    meanStatorTorque(i) = mean(res(i).StatorTorque(end - samplingTime/res(i).deltat ));
    MeanTorque(i) = 0.5 * (meanRotorTorque(i)+meanStatorTorque(i));
    TorqueDiff(i) = meanRotorTorque(i)-meanStatorTorque(i);
end


legendPos = 'sw';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(log10(baseSizes), MeanTorque * 1000, '-xk', 'MarkerSize', markerSize, 'displayname', 'Mean Torque (Stator, Rotor)')
hold on
plot(log10(baseSizes), meanRotorTorque * 1000, ':xb', 'MarkerSize', markerSize, 'displayname', 'Mean Rotor Torque')
hold on
plot(log10(baseSizes), meanStatorTorque * 1000, ':xr', 'MarkerSize', markerSize, 'displayname', 'Mean Stator Torque')
hold on
text(0.85,0.95,sprintf('\\textbf{(%c)}','a'),'fontsize',12,...
    'Units', 'normalized')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)
% ylim([7,12])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')


%% Time series
hold off;
paperfactorX = 0.55*2;
paperfactorY = 0.55*1.3;
markerSize = 3;
legendPos = 'se';

f2 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f2, 'PaperUnits', 'centimeters');
set(f2, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f2, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbols = {'m', 'b', 'y', 'c', 'k', 'r', 'g'};


for i = 1:length(res)
    plot(res(i).t, 1000*(res(i).RotorToque), sprintf('-%s',symbols{i}), ...
        'displayname', sprintf('$ \\hat{\\delta}  = %1.3f$', res(i).baseSize*0.01),...
        'MarkerSize', markerSize );
    hold on

    %plot(res(i).t, 1000*(res(i).StatorTorque), sprintf(':%s',symbols{i}), ...
    %    'displayname', sprintf('$ \\hat{\\delta}  = %1.3f$', res(i).baseSize*0.01),...
    %    'MarkerSize', markerSize );

end

ylim auto

text(0.85,0.95,sprintf('\\textbf{(%c)}','b'),'fontsize',12,...
    'Units', 'normalized')
xlabel('t [s]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)

saveas(f2, sprintf('Output/MeshTimeSeries_%s_%iRPM_',Rotor, RPM), 'pdf')