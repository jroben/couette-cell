clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

ParentStudy = 'MeshStudies';



Studies = {'MeshConvergenceRST', 'MeshConvergenceRST'};
StudyLegends = {'RSM','LES'};
RPMs = [250, 500,750,1000];




startPlotTimes = [4e-3, 4e-3, 4e-3, 4e-3, ...
    4e-3, 4e-3, 4e-3, 4e-3];
endPlotTimes=[0.02, 0.02, 0.02, 0.02,...
    1.2, 1.2, 2.2, 2.2];
deltats = [0.5e-3, 0.5e-3, 0.5e-3, 0.5e-3, ...
    0.5e-3, 0.5e-3, 0.5e-3, 0.5e-3];
samplingTimes = [0.001, 0.001, 0.001, 0.001, ...
    0.5, 0.5, 0.5, 0.5]; %s

colors = {'b', 'r', 'g', 'c', 'k', 'y','m'};
symbols = {'x', 's', 'd', '^'};
lines = {'', '--'};
iRes = 1;

baseSizesStrings(1).arr = {'5'};
baseSizesStrings(2).arr = {'5', '7.5', '10', '15', '20', '30', '40', '50'};
% baseSizesStrings(2).arr = {'5','50'};

for iStudy = 1:length(Studies)
    for iRPM = 1:length(RPMs)
        
        Study = Studies{iStudy};
        Rotor = 'Rotor1';
        if strcmp(Rotor, 'Rotor1')
            RotorDiameter = 0.080;
            CellDiameter = 0.098;
            RotorLength = 0.138;
        else
            RotorDiameter = 0.050;
            CellDiameter = 0.098;
            RotorLength = 0.138;
        end
        RPM = RPMs(iRPM);
        Omega = RPM/60*2*pi;
        
        density = 997.561;
        dynamicViscosity = 8.82E-07*density;
        
        
        kineticViscosity = dynamicViscosity/density;
        
        baseSizesStr = baseSizesStrings(iStudy).arr;
         
        ReynoldsNumbers(iRPM) = Omega*0.5*RotorDiameter*(0.5*(CellDiameter-RotorDiameter))/kineticViscosity;
        
        
        steadyIter = 20000;
        nInnerIter = 5;
        startPlotTime = startPlotTimes(iRes);
        endPlotTime = endPlotTimes(iRes);
        deltat = deltats(iRes);
        samplingTime = samplingTimes(iRes); %s
        
        StatorTorque        = zeros(length(baseSizesStr),1);
        RotorTorque         = zeros(length(baseSizesStr),1);
        MeanTorque          = zeros(length(baseSizesStr),1);
        meanRotorTorque     = zeros(length(baseSizesStr),1);
        meanStatorTorque    = zeros(length(baseSizesStr),1);
        meanRotorOnlyTorque = zeros(length(baseSizesStr),1);
        TorqueDiff          = zeros(length(baseSizesStr),1);
        baseSizes           = zeros(length(baseSizesStr),1);
        
        for iSize = 1:length(baseSizesStr)
            baseSizes(iSize) = str2double(baseSizesStr{iSize});
            
            res(iSize).baseSize = baseSizes(iSize);
            res(iSize).deltat = deltat * (res(iSize).baseSize/20) * ( 0.080/ RotorDiameter);
            res(iSize).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(iSize).deltat/10^(floor(log10(res(iSize).deltat))) ,floor(log10(res(iSize).deltat))  );
            
            fNameStringTime = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/PhysicalTime.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringRotor = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringStator = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringRotorOnly = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorOnlyMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});

            res(iSize).dfR = readTorque(fNameStringRotor);
            res(iSize).dfS = readTorque(fNameStringStator);
            res(iSize).dfT = readPhysicalTime(fNameStringTime);
            res(iSize).dfRO = readTorque(fNameStringRotorOnly);
            
            if(~isempty(res(iSize).dfT))
                nInnerIter = res(iSize).dfT.IterationIteration(2) - res(iSize).dfT.IterationIteration(1);
                steadyIter = res(iSize).dfT.IterationIteration(1) - nInnerIter;
                res(iSize).deltat = res(iSize).dfT.PhysicalTimePhysicalTimes(2) - res(iSize).dfT.PhysicalTimePhysicalTimes(1);
                tsStart = ceil(startPlotTime / res(iSize).deltat);
                tsEnd = ceil(endPlotTime / res(iSize).deltat);
            else 
                nInnerIter = 5;
                steadyIter = 15000;
                tsStart = ceil(startPlotTime / res(iSize).deltat);
                tsEnd = ceil(0.1 / res(iSize).deltat);
                samplingTime = 0.01;
            end
            
            
           
            
            res(iSize).iter = res(iSize).dfR.IterationIteration;
            
            res(iSize).itStart = find(res(iSize).iter == (steadyIter + tsStart * nInnerIter));
            res(iSize).itEnd = find(res(iSize).iter == (steadyIter + tsEnd * nInnerIter));
            
%             res(iSize).timeIter = res(iSize).dfR.IterationIteration(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).timeIter = res(iSize).dfT.IterationIteration;
            res(iSize).timelevel = (  res(iSize).timeIter - steadyIter ) / nInnerIter;
            res(iSize).t = res(iSize).timelevel * res(iSize).deltat;
            
            res(iSize).RotorToque = -res(iSize).dfR.Torque(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).StatorTorque = res(iSize).dfS.Torque(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).RotorOnlyToque = res(iSize).dfRO.Torque(res(iSize).itStart:res(iSize).itEnd);
            
            meanRotorTorque(iSize) = mean(res(iSize).RotorToque(end - floor(samplingTime/res(iSize).deltat )));
            meanStatorTorque(iSize) = mean(res(iSize).StatorTorque(end - floor(samplingTime/res(iSize).deltat )));
            meanRotorOnlyTorque(iSize) = mean(abs(res(iSize).RotorOnlyToque(end - floor(samplingTime/res(iSize).deltat ))));
            MeanTorque(iSize) = 0.5 * (meanRotorTorque(iSize)+meanStatorTorque(iSize));
            TorqueDiff(iSize) = meanRotorTorque(iSize)-meanStatorTorque(iSize);
        end
        T = meanRotorOnlyTorque(1);
        G = T/(density* kineticViscosity.^2 *(RotorLength));
        
        Results(iRes).meanRotorOnlyTorque = meanRotorOnlyTorque;
        Results(iRes).baseSizes = baseSizes;
        Results(iRes).RPM = RPM;
        Results(iRes).T = T;
        Results(iRes).G = G;
        Results(iRes).DisplayName = sprintf('%s, Re = %.1f$\\times 10^4$', StudyLegends{iStudy}, ReynoldsNumbers(iRPM)*1e-4);
        Results(iRes).Color = colors{iRPM};
        Results(iRes).Symbol = symbols{iStudy};
        Results(iRes).Line = lines{iStudy};
        Results(iRes).DisplayType = sprintf('%c%c%c',Results(iRes).Line,Results(iRes).Symbol,Results(iRes).Color);
        iRes = iRes +1;
    end
end

legendPos = 'NorthOutside';
figureName = 'a';


paperfactorX = 0.55*1.25;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',8,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.



for iRes = 1:length(Results)
    semilogx(Results(iRes).baseSizes/100, Results(iRes).meanRotorOnlyTorque, ...
        Results(iRes).DisplayType, 'MarkerSize', markerSize, 'displayname', Results(iRes).DisplayName)
    hold on
end

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized', 'FontName', 'Times New Roman')
xlabel('$\hat{\delta}$ [-]')
ylabel('Torque [Nm]')
ytickformat('%,.0f')
xtickformat('%,.2f')
box on;

ax = gca;
ax.YAxis.Exponent = -3;
% ax.XAxis.Exponent = -3;

legend(gca, 'show', 'location', legendPos,'NumColumns',2)
legend('boxoff')
set(gca, 'FontName', 'Times New Roman')


ylim([0,35]*1e-3)
 xlim([0.05,0.5])
 xticks([0.05,0.1,0.2,0.3,0.4,0.5])
saveas(f1, sprintf('Output/%s_%s',ParentStudy,Rotor), 'pdf')
saveas(f1, sprintf('Output/%s_%s',ParentStudy,Rotor), 'fig')
print(f1,sprintf('Output/%s_%s.png',ParentStudy,Rotor),'-dpng','-r800');
