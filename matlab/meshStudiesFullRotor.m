clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

ParentStudy = 'MeshStudies';

Studies = {'MeshConvergenceRST', 'MeshConvergenceRST'};
StudyLegends = {'RST','LES'};
RPMs = [500,750,1000];


startPlotTimes = [4e-3, 4e-3, 4e-3, ...
    4e-3, 4e-3, 4e-3];
endPlotTimes=[0.02, 0.02, 0.02,...
    2.2, 2.2, 2.2];
deltats = [0.5e-3, 0.5e-3, 0.5e-3, ...
    0.5e-3, 0.5e-3, 0.5e-3];
samplingTimes = [0.001, 0.001, 0.001, ...
    0.5, 0.5, 0.5]; %s

colors = {'b', 'r', 'g', 'c', 'k', 'y','m'};
symbols = {'x', 's', 'd'};
lines = {'', '--'};
iRes = 1;

baseSizesStrings(1).arr = {'5'};
baseSizesStrings(2).arr = {'5', '7.5', '10', '15', '20', '30', '40', '50'};
% baseSizesStrings(2).arr = {'5','50'};

for iStudy = 1:length(Studies)
    for iRPM = 1:length(RPMs)
        
        Study = Studies{iStudy};
        Rotor = 'Rotor1';
        if strcmp(Rotor, 'Rotor1')
            RotorDiameter = 0.080;
            CellDiameter = 0.098;
            RotorLength = 0.138;
        else
            RotorDiameter = 0.050;
            CellDiameter = 0.098;
            RotorLength = 0.138;
        end
        RPM = RPMs(iRPM);
        
        density = 997.561;
        dynamicViscosity = 8.8e-4;
        
        
        kineticViscosity = dynamicViscosity/density;
        
        baseSizesStr = baseSizesStrings(iStudy).arr;
        
        
        
        steadyIter = 20000;
        nInnerIter = 5;
        startPlotTime = startPlotTimes(iRes);
        endPlotTime = endPlotTimes(iRes);
        deltat = deltats(iRes);
        samplingTime = samplingTimes(iRes); %s
        
        StatorTorque        = zeros(length(baseSizesStr),1);
        RotorTorque         = zeros(length(baseSizesStr),1);
        MeanTorque          = zeros(length(baseSizesStr),1);
        meanRotorTorque     = zeros(length(baseSizesStr),1);
        meanStatorTorque    = zeros(length(baseSizesStr),1);
        meanRotorOnlyTorque = zeros(length(baseSizesStr),1);
        TorqueDiff          = zeros(length(baseSizesStr),1);
        baseSizes           = zeros(length(baseSizesStr),1);
        
        for iSize = 1:length(baseSizesStr)
            baseSizes(iSize) = str2double(baseSizesStr{iSize});
            
            res(iSize).baseSize = baseSizes(iSize);
            res(iSize).deltat = deltat * (res(iSize).baseSize/20) * ( 0.080/ RotorDiameter);
            res(iSize).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(iSize).deltat/10^(floor(log10(res(iSize).deltat))) ,floor(log10(res(iSize).deltat))  );
            
            fNameStringTime = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/PhysicalTime.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringRotor = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringStator = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/StatorMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            fNameStringRotorOnly = sprintf('./Data/%s/%s/%iRPM/%s/Results/monitors/RotorOnlyMomentMonitor.csv', Study, Rotor, RPM, baseSizesStr{iSize});
            
            
            res(iSize).dfR = readTorque(fNameStringRotor);
            res(iSize).dfS = readTorque(fNameStringStator);
            res(iSize).dfT = readPhysicalTime(fNameStringTime);
            res(iSize).dfRO = readTorque(fNameStringRotorOnly);
            
            tsStart = ceil(startPlotTime / res(iSize).deltat);
            tsEnd = ceil(endPlotTime / res(iSize).deltat);
            
            res(iSize).iter = res(iSize).dfR.IterationIteration;
            res(iSize).itStart = find(res(iSize).iter == (steadyIter + tsStart * nInnerIter));
            res(iSize).itEnd = find(res(iSize).iter == (steadyIter + tsEnd * nInnerIter));
            
            res(iSize).timeIter = res(iSize).dfR.IterationIteration(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).timelevel = (  res(iSize).timeIter - steadyIter ) / nInnerIter;
            res(iSize).t =  res(iSize).timelevel * res(iSize).deltat;
            
            res(iSize).RotorToque = -res(iSize).dfR.Torque(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).StatorTorque = res(iSize).dfS.Torque(res(iSize).itStart:res(iSize).itEnd);
            res(iSize).RotorOnlyToque = res(iSize).dfRO.Torque(res(iSize).itStart:res(iSize).itEnd);
            
            meanRotorTorque(iSize) = mean(res(iSize).RotorToque(end - floor(samplingTime/res(iSize).deltat )));
            meanStatorTorque(iSize) = mean(res(iSize).StatorTorque(end - floor(samplingTime/res(iSize).deltat )));
            meanRotorOnlyTorque(iSize) = mean(abs(res(iSize).RotorOnlyToque(end - floor(samplingTime/res(iSize).deltat ))));
            MeanTorque(iSize) = 0.5 * (meanRotorTorque(iSize)+meanStatorTorque(iSize));
            TorqueDiff(iSize) = meanRotorTorque(iSize)-meanStatorTorque(iSize);
        end
        T = meanRotorTorque(1);
        G = T/(density* kineticViscosity.^2 *(RotorLength));
        
        Results(iRes).meanRotorOnlyTorque = meanRotorOnlyTorque;
        Results(iRes).meanStatorTorque = meanStatorTorque;
        Results(iRes).baseSizes = baseSizes;
        Results(iRes).RPM = RPM;
        Results(iRes).T = T;
        Results(iRes).G = G;
        Results(iRes).DisplayName = sprintf('%s, %i RPM', StudyLegends{iStudy}, RPM);
        Results(iRes).Color = colors{iRPM};
        Results(iRes).Symbol = symbols{iStudy};
        Results(iRes).Line = lines{iStudy};
        Results(iRes).DisplayType = sprintf('%c%c%c',Results(iRes).Line,Results(iRes).Symbol,Results(iRes).Color);
        iRes = iRes +1;
    end
end

legendPos = 'NorthOutside';
figureName = 'a';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.



for iRes = 1:length(Results)
    semilogx(Results(iRes).baseSizes/100, Results(iRes).meanStatorTorque * 1000, ...
        Results(iRes).DisplayType, 'MarkerSize', markerSize, 'displayname', Results(iRes).DisplayName)
    hold on
end

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized', 'FontName', 'Times New Roman')
xlabel('$\log_{10}(\hat{\delta})$ [-]')
ylabel('Torque [Nmm]')
ytickformat('%,.1f')
xtickformat('%,.2f')

legend(gca, 'show', 'location', legendPos,'NumColumns',2)
legend('boxoff')
set(gca, 'FontName', 'Times New Roman')
ylim([5,40])
saveas(f1, sprintf('Output/%s_%s',ParentStudy,Rotor), 'pdf')
saveas(f1, sprintf('Output/%s_%s',ParentStudy,Rotor), 'fig')
print(f1,sprintf('Output/%s_%s.png',ParentStudy,Rotor),'-dpng','-r800');
