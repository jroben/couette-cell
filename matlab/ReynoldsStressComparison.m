clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'ReynoldsStresses';
Rotor = 'Rotor1';
RPM = 1000;

Omega = RPM/60*2*pi;
RotorDiameter = 0.080;


%% Extract Data
LES = importReynoldsStresses(sprintf('./Data/LES%i_2.csv',RPM));
RST = importReynoldsStresses(sprintf('./Data/RST%i.csv',RPM));


%% Transform
LES.magRST = (sqrt([LES.RST_uu.^2+LES.RST_vv.^2+LES.RST_ww.^2+LES.RST_uv.^2+LES.RST_uw.^2+LES.RST_vw.^2] )) ./ (Omega*0.5*RotorDiameter).^2;
RST.magRST = (sqrt([RST.RST_uu.^2+RST.RST_vv.^2+RST.RST_ww.^2+RST.RST_uv.^2+RST.RST_uw.^2+RST.RST_vw.^2] )) ./ (Omega*0.5*RotorDiameter).^2;
    

%% Plot
legendPos = 'no';
figureName = 'b';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

plot(LES.Zm/0.140, LES.RST_uu, 'xr', 'DisplayName', 'LES')
hold on
plot(RST.Zm/0.140, RST.RST_uu, '.k', 'DisplayName', 'RANS')
    
% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$Z/h$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$||\mathbf{R}||/(\omega r_1)^2$ [-]', 'FontSize', 12, 'FontWeight', 'bold');

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized', 'FontName', 'Times New Roman')

ytickformat('%,.3f')
xtickformat('%,.3f')
legend(gca, 'show', 'location', legendPos,'NumColumns',1)
legend('boxoff')

saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor,RPM),'-dpng','-r800');