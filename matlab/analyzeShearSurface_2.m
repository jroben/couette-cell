clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'ShearStudy';
Rotor = 'Rotor1';
    

RotorDiameter = 0.080;

RPMs = [250,500,750,1000];

density = 997.561;
dynamicViscosity = 8.82E-07*density;
kineticViscosity = dynamicViscosity/density;

if strcmp(Rotor, 'Rotor1')
    RotorDiameter = 0.080;
    CellDiameter = 0.098;
    RotorLength = 0.138;
else
    RotorDiameter = 0.050;
    CellDiameter = 0.098;
    RotorLength = 0.138;
end


        


LES = {};
RST = {};

height = 140e-3;

bins = 350;
zTop = linspace(height/(bins+1), height, bins);
zBottom = zTop - height/(bins+1);

%% Extract Data
for i = 1:length(RPMs)
    LES(i).RPM = RPMs(i);
    LES(i).Omega = LES(i).RPM/60*2*pi;
    LES(i).Re = LES(i).Omega*0.5*RotorDiameter*(0.5*(CellDiameter-RotorDiameter))/kineticViscosity;
    fileNameLES = sprintf('./Data/Shear/%iRPM.csv', LES(i).RPM);
    LES(i).Data = importShearStress(fileNameLES);
    
     RST(i).RPM = RPMs(i);
     RST(i).Omega = RST(i).RPM/60*2*pi;
     fileNameRST = sprintf('./Data/Shear/RST/%iRPM.csv', LES(i).RPM);
     RST(i).Data = importShearStress(fileNameRST);
    
end




%% Transform
for i = 1:length(LES)
    LES(i).shear = zeros(bins,1);
    LES(i).z = 0.5*(zBottom+zTop); 
    RST(i).shear = zeros(bins,1);
    RST(i).z = 0.5*(zBottom+zTop);
     for j = 1:bins
         indsLES = find( (LES(i).Data.Z >= zBottom(j)) .* (LES(i).Data.Z < zTop(j)));
         LES(i).shear(j) = mean(LES(i).Data.Shear(indsLES));
         indsRST = find( (RST(i).Data.Z >= zBottom(j)) .* (RST(i).Data.Z < zTop(j)));
         RST(i).shear(j) = mean(RST(i).Data.Shear(indsRST));
     end
end


%% Plot
legendPos = 'no';
figureName = 'c';
format = 'h'; % 'h' / v

paperfactorX = 0.55 * 2;
paperfactorY = 0.55*1;

marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',8,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.



%  symbolsSim = {'-r', '--b'};
symbolsExp = {'--r', ':b', '-g', '-.m'};
for i=1:length(RPMs)
    plot(LES(i).z/height, LES(i).shear, symbolsExp{i}, 'DisplayName', sprintf('Re = %.1f$\\times 10^4$',LES(i).Re/1e4))
    hold on
end

% symbolsExp = {'--r', ':b', '-g', '-.m'};
% for i=1:length(RPMs)
%     plot(RST(i).z/height, RST(i).shear, symbolsExp{i}, 'DisplayName', sprintf('%i RPM',RST(i).RPM))
%     hold on
% end


% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$Z/h$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$|\tau|$ [Nm$^2$]', 'FontSize', 12, 'FontWeight', 'bold');

%  text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
%      'Units', 'normalized')
 text(1.0,1.1,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
     'Units', 'normalized')

ytickformat('%,.0f')
xtickformat('%,1.1f')
legend(gca, 'show', 'location', legendPos,'NumColumns',2)
legend('boxoff')

xlim([0,1])

saveas(f1, sprintf('Output/%s_%s',Study,Rotor), 'pdf')
saveas(f1, sprintf('Output/%s_%s',Study,Rotor), 'fig')
print(f1,sprintf('Output/%s_%s.png',Study,Rotor),'-dpng','-r800');