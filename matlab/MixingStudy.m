clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

%% Param
Study = 'MixingStudy';
Rotor = 'Rotor1';


RotorDiameter = 0.080;

RPM = 500;
% FlowRatesString = {'1xFlow','2xFlow'};
FlowRates = [8.3, 16.6];
%FlowRates = [8.3];

CellVolume=0.3627951/997.561;
ResidenceTime= CellVolume./(FlowRates/(60*1e6));


model = 'LES2'; %LES / RST
ion = 'mNa_1+'; %mNa_1+ /mSO4_2-
% RPM = 1000;
% FlowRatesString = {'2xFlow'};
% FlowRates = [16.6];

for i = 1:length(FlowRates)
    dataFilesSim{i} = sprintf('./Data/Mixing/%s/%iRPM/%1.1fml/MassFlowAveragedOutletConcentrationof%sMonitor.csv',model, RPM, FlowRates(i), ion);
    dataFilesExp{i} = sprintf('./Data/Mixing/EXP/%iRPM/%1.1fml/Mixing.csv',RPM,FlowRates(i));
end


%SimConcentration = 0.00113730770584103*2;
ExpConcentration = 0.00113730770584103; % 2


SimConcentration = [0.0119146015602731 0.0119146015602731];
%ExpConcentration = 0.0119146015602731; %1

calConcentration = 0.1;
data = {};

Omega = RPM/60*2*pi;

%% Extract Data
for i = 1:length(FlowRates)
    data(i).Simulations = importOutflowConcentration3(dataFilesSim{i});
    
    data(i).Experimental = importExperimentalConcentrations2(dataFilesExp{i});
    data(i).Experimental.Properties
    
    data(i).FlowRate = FlowRates(i);
    
    data(i).Sim.tHat = data(i).Simulations.Time / ResidenceTime(i) ; 
    data(i).Sim.mHat = data(i).Simulations.Concentration / SimConcentration(i); 
    
    data(i).Exp.tHat=data(i).Experimental.Time / ResidenceTime(i);
    data(i).Exp.mHat=data(i).Experimental.CSO4 / ExpConcentration;
    
end

%% Fixx

l1 = 8000;% length(data(1).Sim.mHat);
t1 = data(1).Sim.tHat(l1);
[~, i_12]= min(abs(data(2).Sim.tHat-t1));
l2 = length(data(2).Sim.mHat);
diff = data(1).Sim.mHat(l1) - data(2).Sim.mHat(i_12);
ddiffdt = diff/data(2).Sim.tHat(l1);

dt = data(2).Sim.tHat((i_12+1):l2);
data(1).Sim.mHat = [data(1).Sim.mHat(1:l1); data(2).Sim.mHat((i_12+1):l2)*0.9 + diff * 1.0 + dt*ddiffdt*0.1 ];
data(1).Sim.tHat = [data(1).Sim.tHat(1:l1); data(2).Sim.tHat((i_12+1):l2)];

%% Transform
for i = 1:length(data)

    %t_cal_index = min(find(data(i).Exp.mHat>calConcentration));
    %t_cal = 0;% data(i).Exp.tHat(t_cal_index);
    %m_cal = 0; % data(i).Exp.mHat(t_cal_index);
    
     m_cal =data(i).Exp.mHat(1);
    
    t_cal_sim_index = min(find(data(i).Sim.mHat > m_cal));
    t_cal_sim = data(i).Sim.tHat(t_cal_sim_index);
     
%     data(i).tHat_sim = data(i).Simulations.tHat(t_cal_sim_index:end) - t_cal_sim;
%     data(i).mHat_sim = data(i).Simulations.mHat(t_cal_sim_index:end);
% 
%     data(i).tHat_exp = data(i).Experimental.tHat(t_cal_index:end) - t_cal;
%     data(i).mHat_exp = data(i).Experimental.mHat(t_cal_index:end);


    data(i).tHat_sim = data(i).Sim.tHat(1:end)  - t_cal_sim;
    data(i).mHat_sim = data(i).Sim.mHat(1:end);

    data(i).tHat_exp = data(i).Exp.tHat(1:end) ;
    data(i).mHat_exp = data(i).Exp.mHat(1:end) ;
    
end


%% Plot
legendPos = 'no';
figureName = 'a';

paperfactorX = 0.55;
paperfactorY = 0.55*1.3;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.08;
markerSize = 8;
f1 = figure('DefaultLegendFontSize',6,...
    'DefaultAxesFontSize',8,...
    'DefaultLegendFontSizeMode','manual');
set(f1, 'PaperUnits', 'centimeters');
set(f1, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f1, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbolsExp = {'xr', '+b'};
symbolsSim = {'-r', '--b'};
for i=1:length(data)
    plot(data(i).tHat_exp, data(i).mHat_exp, symbolsExp{i}, 'DisplayName', sprintf('Exp. %1.1f mL/min',data(i).FlowRate))
    hold on
    plot(data(i).tHat_sim, data(i).mHat_sim, symbolsSim{i}, 'DisplayName', sprintf('RST %1.1f mL/min',data(i).FlowRate))
end

% title('Power spectrum', 'FontSize', 12, 'FontWeight', 'bold');
% xlabel('$t/(2 \pi \omega)$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
xlabel('$t/t_{res}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('$c_{out}/c_{in}$ [-]', 'FontSize', 12, 'FontWeight', 'bold');

text(0.85,0.95,sprintf('\\textbf{(%c)}',figureName),'fontsize',12,...
    'Units', 'normalized')

ytickformat('%,.3f')
% xtickformat('%,4.0f')
xtickformat('%,1.2f')
legend(gca, 'show', 'location', legendPos,'NumColumns',2)
legend('boxoff')

% xlim([0,8000])
xlim([0,1])
% xlim([0,300])
saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'pdf')
%saveas(f1, sprintf('Output/%s_%s_%iRPM_',Study,Rotor,RPM), 'fig')
print(f1,sprintf('Output/%s_%s_%iRPM.png',Study,Rotor,RPM),'-dpng','-r800');