clear all; close all; clc;
addpath('miscTools/')
dataFolder = 'Data';


fileNames ={'RotorMoment.csv','StatorMoment.csv'};



simObjects = [];
simObjects(1).simFolderName = 'Les2';
simObjects(1).tStart = 0.4;
simObjects(1).tEnd = 1.6;
simObjects(1).N = 7.4e6;

simObjects(2).simFolderName = 'Les2';
simObjects(2).tStart = 1.7;
simObjects(2).tEnd = 1.96;
simObjects(2).N = 24e6;

for i = 1 : length(simObjects)
    % Generate file paths
    filePaths = {sprintf('%s/%s/%s',dataFolder,simObjects(i).simFolderName,fileNames{1}),...
    sprintf('%s/%s/%s',dataFolder,simObjects(i).simFolderName,fileNames{2})};
    
    % Data reading
    rotorMomentObj = momentsReader(filePaths{1},1);
    statorMomentObj = momentsReader(filePaths{2},1);
    
    % Filter time
    timeIndStart = BinarySearch(rotorMomentObj.PhysicalTimes,simObjects(i).tStart,'firstAbove');
    timeIndEnd = BinarySearch(rotorMomentObj.PhysicalTimes,simObjects(i).tEnd,'firstBelow');
    timeInds = timeIndStart:timeIndEnd;
    time = rotorMomentObj.PhysicalTimes(timeInds);
    rotorMoment = rotorMomentObj.Moment(timeInds);
    statorMoment = statorMomentObj.Moment(timeInds);
    
    % Calculate means
    simObjects(i).meanRotorMoment = mean(rotorMoment);
    simObjects(i).meanStatorMoment = mean(statorMoment);
    simObjects(i).meanAverageMoment = 0.5* (abs(simObjects(i).meanRotorMoment) + abs(simObjects(i).meanStatorMoment));
    
end

%% Plot results
Ns = [simObjects.N];
meanMoments = [simObjects.meanAverageMoment];
f1 = figure;
plot(log10(Ns), log10(meanMoments),'xb','markersize',10)
