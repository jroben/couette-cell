function Shear500 = importShearStress(filename, dataLines)
%IMPORTFILE Import data from a text file
%  SHEAR500 = IMPORTFILE(FILENAME) reads data from text file FILENAME
%  for the default selection.  Returns the data as a table.
%
%  SHEAR500 = IMPORTFILE(FILE, DATALINES) reads data for the specified
%  row interval(s) of text file FILENAME. Specify DATALINES as a
%  positive scalar integer or a N-by-2 array of positive scalar integers
%  for dis-contiguous row intervals.
%
%  Example:
%  Shear500 = importfile("/home/jrb/share/couette-cell/matlab/Data/Shear500.csv", [1, Inf]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 13-Oct-2020 12:13:52

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [1, Inf];
end

%% Setup the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 2);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["Z", "Shear"];
opts.VariableTypes = ["double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
Shear500 = readtable(filename, opts);

end