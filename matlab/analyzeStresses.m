
clear all; close all; clc;
figure 
filename = {'Data/MeshConvergence2/Rotor1_500RPM/3.125/stresses7000/Rotor.csv',...
    'Data/MeshConvergence2/Rotor1_500RPM/3.125/stresses7400/Rotor.csv',...
    'Data/MeshConvergence2/Rotor1_500RPM/3.125/stresses7600/Rotor.csv',...
    'Data/MeshConvergence2/Rotor1_500RPM/3.125/stresses8000/Rotor.csv'};
t = [7000,7600,7800,8000] * 0.001;
for j = 1:length(t)
df = readStresses(filename{j});

z = df.Zm;

tau = zeros(size(z,1),size(z,2));
r = zeros(size(z,1),size(z,2));
rotorH = 138.4e-3;

for i = 1:length(z)
    ShearVec = [df.WallShearStressiPa(i); df.WallShearStressjPa(i);df.WallShearStresskPa(i)];
    rVec = [df.Xm(i);df.Ym(i);df.Zm(i)];
    r(i) = norm(rVec(1:2),2);
    tauVec = cross(rVec,ShearVec);  
    tau(i) = tauVec(3);
end

nBands = 2;
Zs = linspace(min(z), max(z), nBands+1);
ZMeans = 0.5*(Zs(2:end) + Zs(1:end-1));
tauBands = zeros(nBands,1);
Ts = zeros(nBands,1);

for i = 1:nBands
    
    zStart = Zs(i);
    zEnd = Zs(i+1);
    zInds = find((z >= zStart) .* (z < zEnd));
    tauBands(i) = mean(tau(zInds));
    rMean = mean(r(zInds));
    circ = 2 * rMean * pi; 
    Ts(i)=tauBands(i) * circ;
end



plot(ZMeans/rotorH, Ts, ':','displayname', sprintf('t = %1.2f',t(j)))
hold on
end
xlabel('z/h [-]')
ylabel('Torque per length [N]')
legend1 = legend(gca,'show');
set(legend1,'FontSize',25);