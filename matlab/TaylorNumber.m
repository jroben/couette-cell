clc; clear all; close all;


%Input
% RPM = 52.36/2/pi*60;
% RotorDiameter = 0.05;
% StatorDiameter = 0.114;
% DynamicViscosity = 1.7894e-5; %pa s
% Density = 1.225; % kg/m^3

% %Input
RPM = 500;
RotorDiameter = 80/1000;
StatorDiameter = 98/1000;
DynamicViscosity = 8.8871e-4; %pa s
Density = 997.561; % kg/m^3

Omega_1 = RPM/60*2*pi;
Omega_2 = 0;
KinematicViscosity = DynamicViscosity/ Density;

R1 = RotorDiameter/2;
R2 = StatorDiameter/2;
d = R2-R1;
R_mean = 0.5*(R2+R1);

% Dimensionless Numbers
eta = R1/R2;
mu = Omega_2/Omega_1;

fprintf('$\\eta$: %1.2f\n', eta)
fprintf('$\\mu$: %1.2f\n', mu)

Re = Density.*Omega_1.*R1.*d./DynamicViscosity;
%Ta = Omega_1.^2 .* RotorDiameter/2 .* d.^3/KinematicViscosity.^2;

%Ta = 4*Omega_1.^2./KinematicViscosity.^2 .*R1.^4.* ...
%            (1-mu)*(1-mu./(eta.^2)) ...
%            ./(1-eta.^2).^2;

%Ta = 2*eta.^2*d.^4/(1-eta.^2)*(Omega_1/KinematicViscosity).^2;
kappa = DynamicViscosity .*  R1.^2*R2.^2/R_mean.^2;

Ta2 = d.^2 .* R_mean.^2 .* (Omega_1 - Omega_2).^2./(KinematicViscosity.*kappa)
%Ta3 = Omega_1* R1.^2/KinematicViscosity * sqrt((R2-R1)/R1)

fprintf('Taylor Number: %1.1e\n', Ta2)
fprintf('Reynolds Number: %1.1e\n', Re)