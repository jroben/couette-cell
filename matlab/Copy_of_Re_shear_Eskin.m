clear all; 
%this calculates shear stress and Re in the Couette cell
%_1 is for scenario 1, _2 is for scenario 2
react_t_hr = 5;
time = react_t_hr * 3600;
heat_cap = 4184; %heat capacity in J/kg/K
t =[2]; % temperature in celsius
T = t + 273.15; % conversion to Kelvin
d_in_1 = 8; %diameter in cm
r_in_1 = (d_in_1/200); % inner radius in m
d_out = 9.8;%dimaeter in cm
r_out = (d_out/200); % outer radius in m
length_cm = 13.8; %dimaeter in cm
length_metre = length_cm*0.01; % fluid length in metres
eta_1 = (r_in_1/r_out); %radius ratio
aspect_ratio_1 = length_metre/(r_out-r_in_1);
volume_annulus_1 = pi()*length_metre*(r_out^2-r_in_1^2);

%conversion from m3 to ML
volume_1_ml = volume_annulus_1 * 10^6;
%calculation of residence time in Couette Cell
flow = 5:2:200; % flow rate in ml
  tau_1 = volume_1_ml./flow;
  
density = 997.561;% used by simulations
A = 2.44*10^-5; B = 247.8; C  = 140;
dynamic_viscosity = 8.871E-4;% A * 10.^(B./(T-C));
visc = dynamic_viscosity/density; % kinematic viscosity
mass = density * volume_annulus_1;

% eskin G parameters
kappa = 0.44;
xi = 0.406;
phi = (2/(1/eta_1 + eta_1)) - log (1+eta_1/ (1-eta_1));
fun1= (kappa/(1/eta_1 +eta_1))* ((2*pi())^0.5)/(1-eta_1);

    rpm = 0:50:2000;
    Omega_s = 2*rpm* pi()/60;
    Re_1_a = Omega_s * r_in_1*(r_out-r_in_1)/visc (1);
 

  
  
    
% preallocation of arrays
G_eskin_1_a= zeros (1, length(rpm));
 torque_eskin_1_a = zeros(1, length(rpm));
 power_eskin_1_a = zeros(1, length(rpm));
 delta_T_eskin_1_a = zeros(1, length(rpm));
 shear_stress_eskin_1_a = zeros (1, length(rpm));

    
 for i = 1: length(rpm)
    syms g
    
  G_eskin_1_a (i) = double((solve(fun1.*Re_1_a(i) == sqrt(g)*(log(sqrt(g)) + phi + xi), g)));
    torque_eskin_1_a(i) = density* visc(1).^2 *(length_metre)* G_eskin_1_a (i);
    power_eskin_1_a(i) =  torque_eskin_1_a(i) .* Omega_s (i);
    delta_T_eskin_1_a(i) = (power_eskin_1_a(i) .* time)/(heat_cap .* mass);
    shear_stress_eskin_1_a(i) = torque_eskin_1_a(i)/(2*pi()*length_metre*r_out^2);
 
 
end 
 
figure (2)
    plot (rpm, G_eskin_1_a,'k')
    title ('Torque (N.m)')
    xlabel ('Rotation speed (rpm)')
    ylabel('Torque (N.m)')
   hold on 
   hold off

legend('Eskin', 'location', 'best')

% figure (3)
%     plot (w, shear_stress_eskin_1_a,'k')
%     title ('Shear stress')
%     xlabel ('Rotation speed (rpm)')
%     ylabel('Shear stress (Pa)')
%    hold on 
%   plot (w, shear_stress_eskin_1_b, 'b')
%   plot (w, shear_stress_eskin_1_c, 'g')
%   plot (w, shear_stress_eskin_1_d, 'r')
%   plot (w, shear_stress_eskin_2_a,'k--o')
%   plot (w, shear_stress_eskin_2_b, 'b--o')
%    plot (w, shear_stress_eskin_2_c, 'g--o')
%   plot (w, shear_stress_eskin_2_d, 'r--o')
%    hold off
% legend('25C','40C', '70C', '90C','25C 2', '40C 2', '70C 2','90C 2')

% residence time
% figure (4)
%     plot (flow, tau_1,'k')
%     title ('Residence time')
%     xlabel ('Flow rate (ml/min)')
%     ylabel('Residence time (min)')
%    hold on 
%   plot (flow, tau_2, 'r')
%      hold off
% legend('Scenario 1','Scenario 2')