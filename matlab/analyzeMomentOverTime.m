
clear all; close all; clc;
% dfR = readTorque('Data/MeshConvergence2/Rotor1_500RPM/3.125/monitors/RotorMomentMonitor.csv');
% dfS = readTorque('Data/MeshConvergence2/Rotor1_500RPM/3.125/monitors/StatorMomentMonitor.csv');

dfR = readTorque('Data/Les3/Rotor1_500RPM/3.125/monitors/RotorMomentMonitor.csv');
dfS = readTorque('Data/Les3/Rotor1_500RPM/3.125/monitors/StatorMomentMonitor.csv');

steadyIter = 3000 + 50000;
deltat = 0.0001;
innerIter = 5;



iter = dfR.IterationIteration;
itStart = find(iter==steadyIter);
t = (iter(itStart:end)-iter(itStart))/innerIter * deltat;

T = 0.5*(-dfR.Torque(itStart:end)+dfS.Torque(itStart:end));
%T = dfR.Torque(itStart:end)

% Tmean = mean(T(end-3000:end))
Tmean = mean(T(1:end))

figure
plot(t, T, '.k')
hold on
plot([t(1); t(end)], [1;1]*Tmean, '--r')
% xlim([0.0,0.2])
xlabel('Time [s]')
ylabel('Torque [Nmm]')