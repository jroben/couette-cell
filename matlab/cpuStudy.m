clear all; close all; clc;
set(groot, 'DefaultTextInterpreter','latex');
set(groot,'DefaultLegendInterpreter','latex');
set(groot,'DefaultTextFontSize',3)
set(groot,'DefaultLegendFontSize',2);

Study = 'cpuStudy';
Rotor = 'Rotor1';
RPM = 500;
% baseSizesStr = {'3.125','5','6.25','10','12.5','20'};
initialStepStr = { '12000'};
cpu = {'xeon8', 'xeon24','intel8', 'epyc32'};
%cpu = {'intel8', 'epyc32'};
% initialStepStr = {'500'};




nInnerIter = 5;
startPlotTime = 4e-3;
deltat = 2e-3;
samplingTime = 2; %s

StatorTorque = zeros(length(initialStepStr),1);
RotorTorque = zeros(length(initialStepStr),1);
MeanTorque = zeros(length(initialStepStr),1);
meanRotorTorque  = zeros(length(initialStepStr),1);
meanStatorTorque  = zeros(length(initialStepStr),1);
TorqueDiff = zeros(length(initialStepStr),1);
initialSteps = zeros(length(initialStepStr),1);

for j = 1:length(cpu)
    for i = 1:length(initialStepStr)
        initialSteps(i,j) = str2double(initialStepStr{i});
        %res(i,j) = {};
        res(i,j).initialSteps = initialSteps(i,j);
        res(i,j).baseSize = 20;
        res(i,j).deltat = deltat;
        res(i,j).deltatLegendName = sprintf('%1.1f \\cdot 10^{%i}', res(i,j).deltat/10^(floor(log10(res(i,j).deltat))) ,floor(log10(res(i,j).deltat))  );
        res(i,j).cpu = cpu{j};
        
        fNameStringRotor = sprintf('./Data/%s/%s/%s/Results/monitors/RotorMomentMonitor.csv', Study, cpu{j}, initialStepStr{i});
        fNameStringStator = sprintf('./Data/%s/%s/%s/Results/monitors/StatorMomentMonitor.csv', Study, cpu{j}, initialStepStr{i});
        fNameEkinStator = sprintf('./Data/%s/%s/%s/Results/monitors/KineticEngeryMonitor.csv', Study, cpu{j}, initialStepStr{i});
        
        res(i,j).dfR = readTorque(fNameStringRotor);
        res(i,j).dfS = readTorque(fNameStringStator);
        res(i,j).ekin = readTorque(fNameEkinStator);
        
        tsStart = ceil(startPlotTime / res(i,j).deltat);
        
        res(i,j).iter = res(i,j).dfR.IterationIteration;
        res(i,j).itStart = find(res(i,j).iter == (res(i,j).initialSteps + tsStart * nInnerIter));
        
        res(i,j).timeIter = res(i,j).dfR.IterationIteration(res(i,j).itStart:end);
        res(i,j).timelevel = (  res(i,j).timeIter - res(i,j).initialSteps ) / nInnerIter;
        res(i,j).t =  res(i,j).timelevel * res(i,j).deltat;
        
        res(i,j).RotorToque = -res(i,j).dfR.Torque(res(i,j).itStart:end);
        res(i,j).StatorTorque = res(i,j).dfS.Torque(res(i,j).itStart:end);
        res(i,j).KineticEnergy = res(i,j).ekin.Torque(res(i,j).itStart:end);
        
        meanRotorTorque(i,j) = mean(res(i,j).RotorToque(end - samplingTime/res(i,j).deltat ));
        meanStatorTorque(i,j) = mean(res(i,j).StatorTorque(end - samplingTime/res(i,j).deltat ));
        MeanTorque(i,j) = 0.5 * (meanRotorTorque(i,j)+meanStatorTorque(i,j));
        TorqueDiff(i,j) = meanRotorTorque(i,j)-meanStatorTorque(i,j);
    end
end


%% Time series
hold off;
legendPos = 'ne';

paperfactorX = 0.95*1.5;
paperfactorY = 0.55*1.8*1.5;
marginX = 2.2;
marginY = 2.2;
fac1 = 0.01;
fac2 = -0.01;
fac3 = 0.99;
fac4 = 0.99+0.05;
markerSize = 8;

f2 = figure('DefaultLegendFontSize',7,'DefaultAxesFontSize',9,'DefaultLegendFontSizeMode','manual');
set(f2, 'PaperUnits', 'centimeters');
set(f2, 'PaperSize', [(20.5 - 4*marginX)*paperfactorX (20.5 - 4*marginY)*paperfactorY]); %Keep the same paper size
set(f2, 'PaperPosition', [(20.5 - 4*marginX)*paperfactorX*fac1 (20.5 - 4*marginX)*paperfactorY*fac2 (20.5 - 4*marginX)*paperfactorX*fac3 (20.5 - 4*marginX)*paperfactorY*fac4]); %Position the plot further to the left and down. Extend the plot to fill entire paper.

symbols = {'b', 'r', 'y', 'c', 'k', 'm', 'g'};


for i = 1:length(initialStepStr)
    plot(res(i,1).t, 1000*(res(i,1).RotorToque), sprintf(':%s',symbols{1}), ...
        'displayname', sprintf('%s, $N_{steady}  = %1.0f$',res(i,1).cpu, res(i,1).initialSteps),...
        'MarkerSize', markerSize );
    hold on
end

for i = 1:length(initialStepStr)
    plot(res(i,2).t, 1000*(res(i,2).RotorToque), sprintf('-%s',symbols{2}), ...
        'displayname', sprintf('%s, $N_{steady}  = %1.0f$', res(i,2).cpu, res(i,2).initialSteps),...
        'MarkerSize', markerSize );
    hold on
end

for i = 1:length(initialStepStr)
    plot(res(i,3).t, 1000*(res(i,3).RotorToque), sprintf('--%s',symbols{3}), ...
        'displayname', sprintf('%s, $N_{steady}  = %1.0f$',res(i,3).cpu, res(i,3).initialSteps),...
        'MarkerSize', markerSize );
    hold on
end

for i = 1:length(initialStepStr)
    plot(res(i,4).t, 1000*(res(i,4).RotorToque), sprintf('-.%s',symbols{4}), ...
        'displayname', sprintf('%s, $N_{steady}  = %1.0f$', res(i,4).cpu, res(i,4).initialSteps),...
        'MarkerSize', markerSize );
    hold on
end


xlim([0,6])
ylim auto

text(0.85,0.95,sprintf('\\textbf{(%c)}','b'),'fontsize',12,...
    'Units', 'normalized')
xlabel('t [s]')
ylabel('Torque [Nmm]')
ytickformat('%,.3f')
xtickformat('%,.1f')
legend(gca, 'show', 'location', legendPos)

saveas(f2, sprintf('Output/%sTimeSeries_%s_%iRPM_',Study,Rotor, RPM), 'pdf')