#wget https://gitlab.gbar.dtu.dk/jroben/couette-cell/raw/master/src/phiTimeSeriesLes/PhiStudyLes.sh
concentrationNa2SO4=0.00113730770584103
concentrationBaCl2=0.000835878753109641
TurbulentSchmidtNumber=0.75
Temperature=22
SimulationTime=2700
k=__k__


macroFileName="PhiStudyLes.java"

rm *.java
rm *.slurm.*
rm libuser.so
wget -q https://gitlab.gbar.dtu.dk/jroben/couette-cell/raw/master/src/phiTimeSeriesLes/$macroFileName
wget -q https://gitlab.gbar.dtu.dk/jroben/couette-cell/raw/master/src/phiTimeSeriesLes/phiStudyLes.slurm.unix3
wget -q https://gitlab.gbar.dtu.dk/jroben/couette-cell/raw/master/src/phiTimeSeriesLes/phiStudyLes.slurm.xeon40
wget -q https://gitlab.gbar.dtu.dk/jroben/couette-cell/raw/master/src/libuser.so

sed "s/__concentrationNa2SO4__/$concentrationNa2SO4/" -i $macroFileName
sed "s/__concentrationBaCl2__/$concentrationBaCl2/" -i $macroFileName
sed "s/__TurbulentSchmidtNumber__/$TurbulentSchmidtNumber/" -i $macroFileName
sed "s/__Temperature__/$Temperature/" -i $macroFileName
sed "s/__SimulationTime__/$SimulationTime/" -i $macroFileName
sed "s/__RateConstant__/$k/" -i $macroFileName